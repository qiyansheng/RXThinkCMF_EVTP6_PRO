<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_EVTP6_PRO前后端分离旗舰版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2021 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace app\admin\service;

use app\admin\model\File;

/**
 * 文件管理-服务类
 * @author 牧羊人
 * @since 2021/7/10
 * Class FileService
 * @package app\admin\service
 */
class FileService extends BaseService
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2021/7/10
     * FileService constructor.
     */
    public function __construct()
    {
        $this->model = new File();
    }

    /**
     * 获取文件列表
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author 牧羊人
     * @since 2021/7/10
     */
    public function getList()
    {
        // 参数
        $param = request()->param();

        // 查询条件
        $map = [];

        // 文件夹
        $directoryId = isset($param['directoryId']) ? $param['directoryId'] : 0;
        if ($directoryId) {
            $map[] = ["pid", "=", $directoryId];
        } else {
            // 查询一级文件
            $map[] = ['pid', "=", 0];
        }
        $list = $this->model->getList($map, "id asc");
        return message("操作成功", true, $list);
    }

    /**
     * 创建文件夹
     * @return array
     * @throws \think\db\exception\BindParamException
     * @author 牧羊人
     * @since 2021/7/10
     */
    public function saveDir()
    {
        // 参数
        $param = request()->param();
        // 文件夹名称
        $name = trim($param['name']);
        if (!$name) {
            return message("文件夹名称不能为空", false);
        }
        // 目录ID
        $directoryId = getter($param, "directoryId", 0);
        $data = [
            'pid' => $directoryId,
            'name' => $name,
            'directory' => 0,
        ];
        $result = $this->model->edit($data);
        if (!$result) {
            return message("创建文件夹失败", false);
        }
        return message();
    }

}