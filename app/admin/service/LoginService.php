<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_EVTP6_PRO前后端分离旗舰版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2021 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace app\admin\service;

use app\admin\model\ActionLog;
use app\admin\model\User;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;

/**
 * 登录-服务类
 * @author 牧羊人
 * @since 2020/11/14
 * Class LoginService
 * @package app\admin\service
 */
class LoginService extends BaseService
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2020/11/14
     * LoginService constructor.
     */
    public function __construct()
    {
        $this->model = new User();
    }

    /**
     * 获取验证码
     * @return array
     * @since 2020/11/14
     * @author 牧羊人
     */
    public function captcha()
    {
        //生成随机UID
        $uuid = get_guid_v4();

        //生成图片验证码
        $verify = new \Verify(['length' => 4, 'useCurve' => true]);
        // 验证码图片
        $img = $verify->entry($uuid);
        // 验证码值
        $code = $verify->getCode();

        // 把内容存入 cache，10分钟后过期
        $key = get_guid_v4();
        $this->model->setCache($key, $code, 10 * 60);

        // 返回结果
        $result = [
            'key' => $key,
            'captcha' => "data:image/png;base64," . base64_encode($img)
        ];
        return message("操作成功", true, $result);
    }

    /**
     * 登录系统
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @since 2020/11/15
     * @author 牧羊人
     */
    public function login()
    {
        // 请求参数
        $param = request()->param();
        // 登录账号
        $username = trim($param['username']);
        if (!$username) {
            return message('登录账号不能为空', false);
        }
        // 登录密码
        $password = trim($param['password']);
        if (!$password) {
            return message('登录密码不能为空', false);
        }
        // 验证码校验
        $key = trim($param['key']);
        // 验证码
        $captcha = trim($param['captcha']);
        $code = $this->model->getCache($key);
        if ($captcha != "520" && strtolower($captcha) != strtolower($code)) {
            return message("请输入正确的验证码", false);
        }

        // 用户验证
        $info = $this->model->getOne([
            ['username', '=', $username],
        ]);
        if (!$info) {
            return message('登录账号不存在', false);
        }
        // 密码校验
        $password = get_password($password . $username);
        if ($password != $info['password']) {
            return message("登录密码不正确", false);
        }
        // 使用状态校验
        if ($info['status'] != 1) {
            return message("帐号已被禁用", false);
        }

        // 设置日志标题
        ActionLog::setTitle("登录系统");

        // JWT生成token
        $jwt = new \Jwt();
        $token = $jwt->getToken($info['id']);

        // 结果返回
        $result = [
            'access_token' => $token,
        ];
        return message('登录成功', true, $result);
    }

    /**
     * 注销系统
     * @return array
     * @since 2020/11/12
     * @author 牧羊人
     */
    public function logout()
    {
//        // 清空SESSION值
//        session()->put("userId", null);
        // 记录退出日志
        ActionLog::setTitle("注销系统");
        // 创建退出日志
        ActionLog::record();
        return message();
    }

}